from django.shortcuts import render
from django.urls import reverse_lazy
from testapp.models import company
from django.views.generic import ListView,DetailView,CreateView,UpdateView,DeleteView
# Create your views here.

class CompanyListView(ListView):
    model = company

class CompanyDetailView(DetailView):
    model = company

class CompanyCreateView(CreateView):
    model = company
    fields = ('name','location','ceo')

class CompanyUpdateView(UpdateView):
    model = company
    fields = {'name','ceo'}

class CompanyDeleteView(DeleteView):
    model = company
    success_url=reverse_lazy('companies')
