create student project then app process of seting.py
1. create testapp new file forms.py then write,
form django import forms
class studentRegistration(forms.Form):
    name=forms.CharField()
    marks=forms.IntegerField()
2. views.py then,
from django.shortcuts import render
from . import forms
# Create your views here.
def studentregisterview(request):
    form =forms.studentRegistration()
    return render(request,'testApp/register.html',{'form':form})
3. go register.html then,
<!DOCTYPE html>
{% load staticfiles%}
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="{% static "css/demo.css"%}">
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <title></title>
  </head>
  <body>

    <div class="container" align='center'>
     <h1>Student Registeration Form</h1>
    <form  method="post">
        {{form.as_p}}
        {%ccsrf_token%}  <-- very important for data security
      <input type="submit" class="btn btn-primary" value="Register">
    </form>
    </div>
  </body>
</html>

4. url.py AuthenticationMiddlewarefrom django.conf.urls import url
from django.contrib import admin
from testApp import views
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^register/', views.studentregisterview),
]
5. demo.CSS then,

.container
{
  text-align: center;
  font-size: 25px;
  margin-top: 25%;
  color: red;
}
body{
  background: url(https://images.unsplash.com/photo-1501959181532-7d2a3c064642?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=9ffb47d2c8a236bae5e5bb86b2c400b9&auto=format&fit=crop&w=1370&q=80);
}
h1{
  color: red;
}
6. {% csrf_token %}  <-- in our form django will add on hidden form field
