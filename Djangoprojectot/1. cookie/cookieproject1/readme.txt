1. All are same in setting.py
2. go to view.py then,
from django.shortcuts import render

# Create your views here.
def count_view(request):
    count=int(count.COOKIES.get('count',0))+1
    newcount = count+1
    response= render(request,'testapp/count.html',{'count':newcount})
    response.set_cookie('count',newcount)
    #for limited time period COOKIES
    #response.set_cookie('count',newcount,max_age=120)
    return response
3.  Go to count.html then,
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style media="screen">
      span{
        font-size: 200px;
        color: red;
      }
    </style>
  </head>
  <body>
    <h1>Page Count is:<span> {{count}}</span></h1>
  </body>
</html>
4. Go to url.py then,
from django.conf.urls import url
from django.contrib import admin
from testapp import views
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.count_view),
]
