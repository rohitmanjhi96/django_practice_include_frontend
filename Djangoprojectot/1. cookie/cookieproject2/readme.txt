1. startproject
2. Go to view.py then,
from django.shortcuts import render

# Create your views here.
def name_view(request):
    return render(request,'testapp/name.html')

def age_view(request):
    name=request.GET['name']#reading data name.html
    response= render(request,'testapp/age.html')
    response.set_cookie('name',name)
    return response

def gf_view(request):
    age=request.GET['age']
    response=render(request,'testapp/gf.html')
    response.set_cookie('age',age)
    return response

def result_view(request):
    gfname=request.GET['GF']
    name=request.COOKIES.get('name')
    age=request.COOKIES.get('age')
    response=render(request,'testapp/result.html',{'name':name,'age':age,'GF':gfname})
    return response
3. name.html then,
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1>Name Registeraton form</h1>
    <form action='/age'  >
      Enter Name:<input type="text" name="name" value=""><br><br>
      <input type="submit" name="" value="Submit name">
    </form>
  </body>
</html>
4. age.html then,
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1>Age Registeraton form</h1>
    <form action='/gf'>
      Enter Age:<input type="text" name="age" value=""><br><br>
      <input type="submit" name="" value="Submit age">
    </form>
  </body>
</html>
5. gf.html then,
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1>Age Registeraton form</h1>
    <form action='/gf'>
      Enter Age:<input type="text" name="age" value=""><br><br>
      <input type="submit" name="" value="Submit age">
    </form>
  </body>
</html>

6. result.html then,
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1>Thanks for providing info Thanks and check</h1>
    <ul>
      <li>Nmae:{{name}}</li>
      <li>Age:{{age}}</li>
      <li>Gf:{{GF}}</li>
    </ul>
  </body>
</html>
7. url.py then,
from django.conf.urls import url
from django.contrib import admin
from testapp import views
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.name_view),
    url(r'^age/', views.age_view),
    url(r'^gf/', views.gf_view),
    url(r'^result/', views.result_view),
]
