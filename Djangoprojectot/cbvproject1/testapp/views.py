from django.shortcuts import render
from django.views.generic import View,TemplateView
from django.http import HttpResponse
# Create your views here.
class Helloworldview(View):
    def get(self,request):
            return HttpResponse('<h1> This is class based view </h1> ')

class Helloworldtemplateview(TemplateView):
    template_name = 'testapp/result.html'
#TemplateView or template_name its fix and mandatory
class HelloworldContextview(TemplateView):
     template_name = 'testapp/info.html'

     def get_context_data(self,**kwargs):   #this function called by django itself
         context = super().get_context_data(**kwargs)
         context['name'] = 'Rohit'
         context['subject'] = 'Python'
         context['marks'] = 99
         return context
