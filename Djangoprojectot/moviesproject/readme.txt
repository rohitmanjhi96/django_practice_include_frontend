1. create project moviesproject
2. startapp testApp
3. setting all configureation
4. static and template folder creations
5. Go models in testApp
from django.db import models

# Create your models here.
class Movie(models.Model):
    rdate=models.DateField()
    moviename=models.CharField(max_length=30)
    hero=models.CharField(max_length=30)
    rating=models.IntegerField()
6. makemigrations
7. migrate
8. register model in admin.py
from django.contrib import admin
from testApp.models import Movie
# Register your models here.
class MovieAdmin(admin.ModelAdmin):
    list_display=['rdate','moviename','hero','heroine','rating']
    #list_display its predefine class in models
admin.site.register(Movie,MovieAdmin)
9. createsuoeruser
