After setting.py
1.Create forms.py in testapp then,
from django import forms

class Nameform(forms.Form):
    name=forms.CharField()

class AgeForm(forms.Form):
    age=forms.IntegerField()

class GFForm(forms.Form):
    gf=forms.CharField()
2. views.py then,
from django.shortcuts import render
from testapp.forms import Nameform,AgeForm,GFForm
# Create your views here.
def name_view(request):
    form=Nameform()
    return render(request,'testapp/name.html',{'form':form})

def age_view(request):
    name=request.GET['name']
    request.session['name']=name #store in future purpose
    form=AgeForm()
    return render(request,'testapp/age.html',{'form':form})

def gf_view(request):
    age=request.GET['age']
    request.session['age']=age #store in future purpose
    form=GFForm()
    return render(request,'testapp/gf.html',{'form':form})

def result_view(request):
    gf=request.GET['gf']
    request.session['gf']=gf #store in future purpose

    return render(request,'testapp/result.html')
3. name.html then,
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1>Name Registeration Form</h1>
    <form action="/age" >
      {{form.as_p}}
      {%csrf_token%}
      <input type="submit" name="" value="Submit Name">
    </form>
  </body>
</html>
4. age.html then,
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1>Age Registeration Form</h1>
    <form action="/gf" >
      {{form.as_p}}
      {%csrf_token%}
      <input type="submit" name="" value="Submit Name">
    </form>
  </body>
</html>
5.. gf.html then,
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1>GF Registeration Form</h1>
    <form action="/result" >
      {{form.as_p}}
      {%csrf_token%}
      <input type="submit" name="" value="Submit GF Name">
    </form>
  </body>
</html>
6. result.html then,
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1>Thanks for providing information</h1>
    <ul>
      {% for key,value in request.session.items%}
      <li><h2> {{Key|upper}}:{{value|title}}</h2></li>
      {%endfor%}
    </ul>
  </body>
</html>
7. url.py
from django.conf.urls import url
from django.contrib import admin
from testapp import views
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.name_view),
    url(r'^age/', views.age_view),
    url(r'^gf/', views.gf_view),
    url(r'^result/', views.result_view),
]
8. py manage.py migrate
9. runserver
