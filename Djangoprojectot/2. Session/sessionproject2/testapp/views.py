from django.shortcuts import render
from testapp.forms import Nameform,AgeForm,GFForm
# Create your views here.
def name_view(request):
    form=Nameform()
    return render(request,'testapp/name.html',{'form':form})

def age_view(request):
    name=request.GET['name']
    request.session['name']=name #store in future purpose
    form=AgeForm()
    return render(request,'testapp/age.html',{'form':form})

def gf_view(request):
    age=request.GET['age']
    request.session['age']=age #store in future purpose
    form=GFForm()
    return render(request,'testapp/gf.html',{'form':form})

def result_view(request):
    gf=request.GET['gf']
    request.session['gf']=gf #store in future purpose

    return render(request,'testapp/result.html')
