same setting.py
1. Got to view.py then
from django.shortcuts import render

# Create your views here.
def page_count_view(request):
    count=request.session.get('count',0)
    newcount=count+1
    print(request.session.get_expiry_age())
      print(request.session.get_expiry_date())
    request.session['count']=newcount
    return render(request,'testapp/count.html',{'count':newcount})
2. go to count.html then,
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
  <title></title>
<style>
  span{
    font-size: 200px;
    color: red;
  }
</style>
  </head>
  <body>
    <h1>The page count is: <span>{{count}}</span></h1>
  </body>
</html>
3. go to url.py
from django.conf.urls import url
from django.contrib import admin
from testapp import views
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.page_count_view),
]
4. py manage.py migrate
5. py manage.py runserver
