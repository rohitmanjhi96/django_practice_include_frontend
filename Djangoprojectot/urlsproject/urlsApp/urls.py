from django.conf.urls import url
from urlsApp import views

urlpatterns=[
   url(r'^hydjobs/', views.hydjobsinfo),
   url(r'^punjobs/', views.punjobsinfo),
   url(r'^mumjobs/', views.mumjobsinfo),
   url(r'^gwljobs/', views.gwljobsinfo),

]
