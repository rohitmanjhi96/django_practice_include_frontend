from django.shortcuts import render
from testapp.models import Book
from django.views.generic import ListView,DetailView
# Create your views here.
#def info_view(request):
#    books=Book.objects.all()
#    return render(request,'testapp/info.html',{'books':books})

class BookListView(ListView):
    model=Book
    #default template: Book_List.html
    #default context object : Book_List
    #or
    #template_name ='testapp/books.html'
    #context_objects_name = 'list_of_books'

class BookDetailView(DetailView):
    model=Book
    #default template name: book_detail.html
    #default context : book or object
