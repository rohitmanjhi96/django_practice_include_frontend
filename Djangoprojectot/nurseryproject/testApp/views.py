from django.shortcuts import render
import datetime
# Create your views here.
def dateinfo(request):
    date=datetime.datetime.now()
    guest="rohit"
    msg=''
    h=int(date.strftime('%H'))
    if h<12:
        msg='Good morning'
    elif h<16:
        msg="Good Afternoon"
    elif h<22:
        msg="Good Evening"
    else:
        msg = "Good night"
    my_dict={'msg':date,'name':guest,'msg':msg}
    return render(request,'testApp/home.html',context=my_dict)
