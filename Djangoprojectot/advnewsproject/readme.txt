1. after setting.py
2. create template file and under fultilpe file base,home,movies,sports_view
   In base.html
   <!DOCTYPE html>
   {% load staticfiles%}
   <html lang="en" dir="ltr">
   <!-- Latest compiled and minified CSS -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
   <link rel="stylesheet" href="{% static "css/advnews.css"}">
     <head>
       <meta charset="utf-8">
       <title></title>
     </head>
     <body>

       <nav class="navbar navbar-default navbar-inverse">
       <div class="container-fluid">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
             <a class="navbar-brand" href="/">RaFTAR</a>
         </div>

         <!-- Collect the nav links, forms, and other content for toggling -->
           <ul class="nav navbar-nav">
             <li class="active"><a href="/">HOME <span class="sr-only">(current)</span></a></li>
             <li><a href="/movies">MOVIES</a></li>
             <li><a href="/sports">SPORTS</a></li>
             <li><a href="/politics">POLITICS</a></li>

           </ul>


     </nav>
     {% block body_block %}

     {% endblock%}
     </body>
   </html>
3. Home.html then,
<!DOCTYPE html>
{% extends 'testApp/base.html'%}
  {% block body_block %}
     <h1>Welcome to Raftar News Portal</h1>
  {% endblock%}
4. views.py then,
from django.shortcuts import render

# Create your views here.
def home_view(request):
    return render(request,'testApp/home.html')

def movies_view(request):
    return render(request,'testApp/movies.html')

def politics_view(request):
    return render(request,'testApp/politics.html')

def sports_view(request):
    return render(request,'testApp/sports.html')
5. urls.py
rom django.conf.urls import url
from django.contrib import admin
from testApp import views
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.home_view),
    url(r'^movies/', views.movies_view),
    url(r'^sports/', views.sports_view),
    url(r'^politics/', views.politics_view),
]
