from django.shortcuts import render

# Create your views here.
def home_view(request):
    return render(request,'testApp/home.html')

def movies_view(request):
    return render(request,'testApp/movies.html')

def politics_view(request):
    return render(request,'testApp/politics.html')

def sports_view(request):
    return render(request,'testApp/sports.html')
