After create project the go to models
1. Models then,
from django.db import models

# Create your models here.
class FilterModel(models.Model):
    name=models.CharField(max_length=30)
    subjects=models.CharField(max_length=30)
    dept=models.CharField(max_length=30)
    date=models.DateField()
2. py manage.py makemigrations
3. py manage.py migrate
4. Go to admin.py then,
from django.contrib import admin
from testApp.models import FilterModel

# Register your models here.
class FilterModelAdmin(admin.ModelAdmin):
    list_display=['name','subjects', 'dept','date']

admin.site.register(FilterModel,FilterModelAdmin )
5. py manae.py createsuperuser
6. Views .py
from django.shortcuts import render
from testApp.models import FilterModel

# Create your views here.
def upper_view(request):
    data_list=FilterModel.objects.all()
    return render(request,'testApp/upper.html',{'data_list':data_list})
7. upper.html AuthenticationMiddleware<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
  {% load cust_filters%}
    {% for r in data_list%}
    <ul>
      <li>{{r.name|upper}}</li>//<li>{{r.name|upper||add=" Pvt Limited"}}</li>
      <li>{{r.subjects|upper}}</li>//After load cust file <li>{{r.subjects|upper}}</li> then use it
      <li>{{r.dept|upper}}</li>
      <li>{{r.date|upper}}</li>//<li>{{r.date|date:'d-m-y'}}</li>//<li>{{r.date|timesince}}</li>
    </ul><hr>
    {%endfor %}
  </body>
</html>
8. urls.py then,
from django.conf.urls import url
from django.contrib import admin
from testApp import views
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^upper/', views.upper_view),
]
9. How to define our own TEMPLATES
   In testApp create folder "templatetags" and under this one file "__intit__.py"  <== THIS REASON IT IS CONSIDER AS PACKAGE
   and again this folder yemplatetags one new file create "cust_filters.py"
   Inthis file
   from django import template
   register=template.Library()

   def truncate5(value):
       result=value[0:5]
       return result

       def truncate_n(value,n):
           result=value[0:n]
           return result

       register.filter('truncate5',truncate5)
       register.filter('t_n',truncate_n)



10. Inside trmplate file we have to load this filter files
    {% load cust_filters%}
