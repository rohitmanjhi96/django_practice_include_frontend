All things are same then
1. create forms.py in testApp then write code in forms.py,
from django import forms
class FeedBackForm(forms.Form):
    name=forms.CharField()
    rollno=forms.IntegerField()
    email=forms.EmailField()
    feedback=forms.CharField(widget=forms.Textarea)

    def clean_name(self):
        inputname=self.cleaned_data['name']
        print('Validating name')
        if len(inputname)<=4:
            raise forms.ValidationError('The length of name filed should <=4')
        return inputname


    def clean_rollno(self):
        inputrollno=self.cleaned_data['rollno']
        print('Validating rollno')
        return inputrollno

    def clean_email(self):
        inputemail=self.cleaned_data['email']
        print("Validating Email")
        return inputemail

        def clean_feedback(self):
            inputfeedback=self.cleaned_data['feedback']
            print('Validating feedback')
            return inputfeedback

2. Go to views.py then,
from django.shortcuts import render
from . import forms

def thankyou_view(request):
    return render(request,'testApp/thankyou.html')
# Create your views here.
def feedback_view(request):
    form=forms.FeedBackForm()
    if request.method=='POST':
        form=forms.FeedBackForm(request.POST)
        if form.is_valid():
            print('Form validation success and printing feedback')
            print('Student Name:',form.cleaned_data['name'])
            print('Student Rollno:',form.cleaned_data['rollno'])
            print('Student Email Id:',form.cleaned_data['email'])
            print('Student Feedback:',form.cleaned_data['feedback'])
            return thankyou_view(request)
    return render(request,'testApp/feedback.html',{'form':form})

3. <!DOCTYPE html>
{%load staticfiles%}
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="{% static "css/fb.css"%}">
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title></title>
  </head>
  <body>
    <div class="container">
      <h1>Student Feedback Form</h1>
      <form  method="post">
        {{form.as_p}}
        {% csrf_token%}
        <input type="submit" class="btn btn-primary" value="SubmitFeedback">
      </form>
    </div>
  </body>
</html>

4. url.py then,
from django.conf.urls import url
from django.contrib import admin
from testApp import views
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^feedback/', views.feedback_view),
    url(r'^thnks/', views.thankyou_view),

]
5. thankyou.html
<!DOCTYPE html>
{%load staticfiles%}
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="{% static "css/fb.css"%}">
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title></title>
  </head>
  <body>
    <div class="container">
      <h1>Thanks</h1>

    </div>
  </body>
</html>




*************New Topic**************
1. forms.py define inbilt validators
from django import forms
from django.core import validators
class FeedBackForm(forms.Form):
    name=forms.CharField()
    rollno=forms.IntegerField()
    email=forms.EmailField()
    feedback=forms.CharField(widget=forms.Textarea,validators=[validators.MaxLengthValidator(40),validators.MinLengthValidator(10)])

2. from django import forms
from django import forms
from django.core import validators

def starts_with_name(value):
    if value[0].lower() !='d':
        raise forms.ValidationError('Name should be start with d')

def mail(value):
    if value[len(value)-9:] !='gmail.com':
        raise forms.ValidationError('Must be gmail')

class FeedBackForm(forms.Form):
    name=forms.CharField(validators=[starts_with_name])
    rollno=forms.IntegerField()
    email=forms.EmailField(validators=[mail])
    feedback=forms.CharField(widget=forms.Textarea,validators=[validators.MaxLengthValidator(40),validators.MinLengthValidator(10)])

3. use of clean method then,
from django import forms
from django.core import validators


class FeedBackForm(forms.Form):
    name=forms.CharField()
    rollno=forms.IntegerField()
    email=forms.EmailField()
    feedback=forms.CharField(widget=forms.Textarea,validators=[validators.MaxLengthValidator(40),validators.MinLengthValidator(10)])

    def clean(self):
        print("Total form calidation...")
        cleaned_data=super().clean()
        inputname=cleaned_data['name']
        if len(inputname)<10:
             raise forms.ValidationError("anem should compulsary 10 character")
        inputrollno=cleaned_data['rollno']
        if len(str(inputrollno)) !=3:
             raise forms.ValidationError('Roll no must be only 3 digit')

4. py manage.py migrate
5. py manage.py createsuperuser

6. Its type to check password then,

from django import forms
from django.core import validators


class FeedBackForm(forms.Form):
    name=forms.CharField()
    rollno=forms.IntegerField()
    email=forms.EmailField()
    password=forms.CharField(widget=forms.PasswordInput)
    rpassword=forms.CharField(widget=forms.PasswordInput)
    feedback=forms.CharField(widget=forms.Textarea,validators=[validators.MaxLengthValidator(40),validators.MinLengthValidator(10)])

    def clean(self):
        print("Total form calidation...")
        cleaned_data=super().clean()
        inputpwd=cleaned_data['password']
        inputrpwd=cleaned_data['rpassword']
        if inputpwd != inputrpwd:
            raise forms.ValidationError("not match password")
        inputname=cleaned_data['name']
        if len(inputname)<10:
             raise forms.ValidationError("anem should compulsary 10 character")
        inputrollno=cleaned_data['rollno']
        if len(str(inputrollno)) !=3:
             raise forms.ValidationError('Roll no must be only 3 digit')
             
