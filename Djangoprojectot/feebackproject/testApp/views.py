from django.shortcuts import render
from . import forms

def thankyou_view(request):
    return render(request,'testApp/thankyou.html')
# Create your views here.
def feedback_view(request):
    if request.method=='GET':
        form=forms.FeedBackForm()
        return render(request,'testApp/feedback.html',{'form':form})
    if request.method=='POST':
        form=forms.FeedBackForm(request.POST)
        if form.is_valid():
            print('Form validation success and printing feedback')
            print('Student Name:',form.cleaned_data['name'])
            print('Student Rollno:',form.cleaned_data['rollno'])
            print('Student Email Id:',form.cleaned_data['email'])
            print('Student Feedback:',form.cleaned_data['feedback'])
            return thankyou_view(request)
    return render(request,'testApp/feedback.html',{'form':form})
