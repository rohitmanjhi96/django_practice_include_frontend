from django import forms
from django.core import validators


class FeedBackForm(forms.Form):
    name=forms.CharField()
    rollno=forms.IntegerField()
    email=forms.EmailField()
    password=forms.CharField(widget=forms.PasswordInput)
    rpassword=forms.CharField(widget=forms.PasswordInput)
    feedback=forms.CharField(widget=forms.Textarea,validators=[validators.MaxLengthValidator(40),validators.MinLengthValidator(10)])

    def clean(self):
        print("Total form calidation...")
        cleaned_data=super().clean()
        inputpwd=cleaned_data['password']
        inputrpwd=cleaned_data['rpassword']
        if inputpwd != inputrpwd:
            raise forms.ValidationError("not match password") 
        inputname=cleaned_data['name']
        if len(inputname)<10:
             raise forms.ValidationError("anem should compulsary 10 character")
        inputrollno=cleaned_data['rollno']
        if len(str(inputrollno)) !=3:
             raise forms.ValidationError('Roll no must be only 3 digit')
