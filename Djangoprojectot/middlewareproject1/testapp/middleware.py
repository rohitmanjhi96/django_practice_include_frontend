class ExecutionFlowMiddleware(object):
    def __init__(self,get_response):
        self.get_response= get_response

    def __call__self(self,request):
        print("This line printed at pre processing request")
        response=self.get_response(request)
        print("This line printed at post processing of request")
        return response
