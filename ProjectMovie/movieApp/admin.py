from django.contrib import admin
from movieApp.models import Movie
# Register your models here.
class MovieAdmin(admin.ModelAdmin):
    list_display=['moviename','hero','heroin','rating']
admin.site.register(Movie,MovieAdmin)
