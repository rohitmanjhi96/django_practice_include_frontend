from django.db import models
import os


def get_image_path(instance, filename):
    return os.path.join('photos', str(instance.id), filename)


class Alumni_tb(models.Model):

    alumni_id = models.AutoField(primary_key=True)
    fname = models.CharField(max_length=100)
    lname = models.CharField(max_length=100)
    gender = models.CharField(max_length=1)
   # profile_pic = models.ImageField(upload_to=get_image_path, blank=True, null=True)

    fb_link = models.TextField()
    linkedin_link = models.TextField()

    study_or_employee = models. CharField(max_length=1)

    hometown = models . CharField(max_length= 100)
    current_location = models .CharField (max_length =100)

    #password = models.TextField()

    mobile_number = models.CharField(max_length=20)
    year_of_passing = models.IntegerField(max_length=5)
    usn = models.CharField(max_length=20)
    entrepreneur = models.BooleanField()

    year_of_joining = models . IntegerField(max_length=5)

    branch = models.CharField(max_length=5)





