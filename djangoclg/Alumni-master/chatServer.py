import asyncio
import websockets
import sqlite3
#import os
#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Alumni")
#from Home import models
#print(dir(settings.BASE_DIR),settings.BASE_DIR)
clients=[]
db=sqlite3.connect('chat.db')
cursor=db.cursor()
async def hello(websocket, path):
    clients.append(websocket)
    print(websocket,"\n is connected to chat....")
    while True:
        try:
            msg = await websocket.recv()
            try:
                user,message=msg.split('<<<:>>>')
                cursor.execute('insert into chat(Username,msg) values("'+str(user)+'","'+str(message)+'")')
                db.commit()
            except:
                pass
            #Home.models.chat(Username='Lokesh Bhoyar',Messege=msg).save()
        except:
            break
        for client in clients:
            if client != websocket:
                await client.send(msg)
    clients.remove(websocket)
    print(websocket,"\n is disconnected from chat....")
start_server = websockets.serve(hello, '0.0.0.0', 9000)
asyncio.get_event_loop().run_until_complete(start_server)
print('Server started  localhost:9000...\nServer Logs....\n')
try:
    asyncio.get_event_loop().run_forever()
except:
    print('Server Closed')
db.close()
