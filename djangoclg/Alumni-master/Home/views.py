from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from .models import StudentInfo,AlumniInfo,StaffInfo
from django.contrib import auth
import django_socketio
from .forms import StaffInfo_form,AlumniInfo_form,StudentInfo_form
import sqlite3
import smtplib
import base64
state=['Andaman and Nicobar (UT)','Arunachal Pradesh','Assam','Bihar','Chhattisgarh','Goa','Gujarat','Haryana','Himachal Pradesh','Jammu &amp; Kashmir','Jharkhand','Karnataka','Kerala','Madhya Pradesh','Maharashtra','Manipur','Meghalaya','Mizoram','Nagaland','Orissa','Punjab','Rajasthan','Sikkim','Tamil Nadu','Tripura','Uttar Pradesh','Uttarakhand','West Bengal','Delhi','Andhra Pradesh','Chandigarh (UT)','Dadra and Nagar Haveli (UT)','Daman and Diu (UT)','Lakshadweep (UT)','Pondicherry (UT)','Telangana','Others']
# Create your views here.
def Welcome(request): #This is Simple Start Welcome Page.........
    return render(request,"Welcome.html",{})
def LAuth(request,color,statement1,statement2,iitbhu='AccessPage'):  #This is informative page for Redirect...........
    if request.user.id:
        return render(request,"LoginAuth.html",{'creg':'blue','hreg':'','reg':'Welcome, '+request.user.first_name,'log':'LOGOUT','color':color,'statement1':statement1,'statement2':statement2,'iitbhu':iitbhu})
    else:
        return render(request,"LoginAuth.html",{'creg':'22bb22','hreg':'../Register/','reg':'REGISTRATION','log':'LOGIN','color':color,'statement1':statement1,'statement2':statement2,'iitbhu':iitbhu})  
def Handle_Pages(request,PageName,title,shade):  #it Handel All Pages.....
    if request.user.id:
        return render(request,PageName,{'creg':'blue','hreg':'','reg':'Welcome, '+request.user.first_name,'log':'LOGOUT','iitbhu':title,shade:'homet'})
    else:
        return render(request,PageName,{'creg':'22bb22','hreg':'../Register/','reg':'REGISTRATION','log':'LOGIN','iitbhu':title,shade:'homet'})
def Home(request):
    return Handle_Pages(request,"Home.html",'IIT BHU(VARANASI) ALUMNI','home')       
def Reunion(request):
    if request.method=='POST':
        return LAuth(request,'red','Sorry Unable To Send Mail Now Check Internet Connection...','Please Try to Search Again....')
    return Handle_Pages(request,"reunion.html","IIT BHU(Varanasi) ALUMNI REUNION",'reunion')  
def Gallery(request):
    return Handle_Pages(request,"Gallery.html",'IIT BHU(Varanasi) Gallery','gallery')
def AlumniMeet(request):
    return Handle_Pages(request,"alumnimeet.html",'IIT BHU(Varanasi) ALUMNI MEET','meet')
def Sendmail(request):
    if request.method=='POST':
        sender='smtplibproject@gmail.com'
        Name=request.POST.get('Name')
        actual_sender=request.POST.get('Email')
        receiever=[]
        receiever.append(str(request.POST.get('Receiever')))
        password='45201017'
        subject=request.POST.get('Subject')
        message=request.POST.get('Message')
        server='smtp.gmail.com'
        port=587
        marker = "AUNIQUEMARKER"
        part1 = """From: From Person <%s>
        To: To Person %s
        Subject: %s
        MIME-Version: 1.0
        Content-Type: multipart/mixed; boundary=%s
        --%s
        """ % (sender,receiever,subject,marker, marker)
        message =part1
        try:
            print('Try to Connecting Server....wait')
            ser=smtplib.SMTP(server,port)
            print('server Connect..')
            ser.ehlo()
            print('ping done')
            ser.starttls()
            print('tls done')
            ser.login(sender,password)
            print('login done')
            ser.sendmail(sender,receiever,str(message))
            print ("Successfully sent email")   
        except Exception:
            return LAuth(request,'red','Sorry Unable To Send Mail Now Check Internet Connection...','Please Try to Search Again....')
    else:
        return LAuth(request,'red','Sorry Unable To Send Mail Now Check Internet Connection...','Please Try to Search Again....')
    return LAuth(request,'green','Message Sent Successfully','Back To Home')

def AlumniProfile(request):
    try:
        data=AlumniInfo.objects.filter(First_Name=request.POST.get('searchbox'),Passout_Year=request.POST.get('batch'),Department=request.POST.get('branch'))[0]
    except:
        return LAuth(request,'red','Sorry This Alumni Not Found','Please Try to Search Again....')
    return render(request,'Profiles.html',{'data':data})
def ContactUs(request):
    return Handle_Pages(request,"contact us.html",'IIT BHU(Varanasi) ContactUs','contact')  
def Register(request):
    return Handle_Pages(request,"Register.html",'IIT(BHU) Varanasi Registration','register')
'''def RegisterData(request,formField,users):
    data=[]
    if request.method == 'POST':
        usertype={'alumni':AlumniInfo,'staff':StaffInfo,'student':StudentInfo}[users]
        for name in formField:
            data.append(request.POST.get(name,''))
        usertype=user(EnrollmentNumber=int(data[0]),Campus=data[1],Department=data[2],Program=data[3],PassoutYear=int(data[4]),Fname=data[5],Lname=data[6],Gender=data[7],MartialStatus=data[8],DOB=data[9],Mob=int(data[10]),EmailId=data[11],CurrentStatus=data[12],Address=data[13]+'\n'+data[14],State=state[int(data[15])-1],Country=data[16],PinCode=int(data[17]),Organization=data[19],Designation=data[20])
        try: #Register Use in auth table............
                newuser=auth.models.User.objects.create_user(data[11],data[11],data[18])
                newuser.first_name=data[5]
                newuser.last_name='alumni'
                newuser.save()
        except:
            return LAuth(request,'red','This Email_Id Alrady Exist','Please Try Register As Another Email_Id')
        AI.save()
        AlumniLogin(info=AI,Username1=data[11],Username2=int(data[0])).save()
        return LAuth(request,'green','Welcome '+data[5]+' '+data[6],'You Are Registerd Successfuly')
    return render(request,"RegisterAlumni.html",{'iitbhu':'IIT(BHU) Varanasi Registration'})
def RAlumni(request):
    data=[]
    if request.method == 'POST':
        form=['EnrollmentNo','Campus','Department','Program','PassOutYear','Fname','Lname','sex','Marital','DOB','Mobile','Email','Status','Addr1','Addr2','State','Country','PinCode','Password','Organisation','Designation']
        for name in form:   
            data.append(request.POST.get(name,''))
        #save data into Alumni Info Table.........
        AI=AlumniInfo(EnrollmentNumber=int(data[0]),Campus=data[1],Department=data[2],Program=data[3],PassoutYear=int(data[4]),Fname=data[5],Lname=data[6],Gender=data[7],MartialStatus=data[8],DOB=data[9],Mob=int(data[10]),EmailId=data[11],CurrentStatus=data[12],Address=data[13]+'\n'+data[14],State=state[int(data[15])-1],Country=data[16],PinCode=int(data[17]),Organization=data[19],Designation=data[20])
        try: #Register Use in auth table............
            newuser=auth.models.User.objects.create_user(data[11],data[11],data[18])
            newuser.first_name=data[5]
            newuser.last_name='alumni'
            newuser.save()
        except:
            return LAuth(request,'red','This Email_Id Alrady Exist','Please Try Register As Another Email_Id')
        AI.save()
        AlumniLogin(info=AI,Username1=data[11],Username2=int(data[0])).save()
        return LAuth(request,'green','Welcome '+data[5]+' '+data[6],'You Are Registerd Successfuly')
    return render(request,"RegisterAlumni.html",{'iitbhu':'IIT(BHU) Varanasi Registration'})
'''
def RAll(request,user):
    forms={'alumni':AlumniInfo_form,'staff':StaffInfo_form,'student':StudentInfo_form}
    if request.method == 'POST':
        form=forms[user](data=request.POST)
        if form.is_valid():
            form.save()
            try:
                #StaffLogin(info=form,Username=request.POST.get('Email_Id')).save()
                newuser=auth.models.User.objects.create_user(request.POST.get('Email_Id'),request.POST.get('Email_Id'),request.POST.get('Password'))
                newuser.first_name=request.POST.get('First_Name')
                newuser.last_name=user
                newuser.save()
            except:
                return LAuth(request,'red','This Email_Id Alrady Exist','Please Try Register As Another Email_Id')            
        return LAuth(request,'green','Welcome '+request.POST.get('First_Name')+' '+request.POST.get('Last_Name'),'You Are Registerd Successfuly')
    form=forms[user]()
    return render(request,'RegisterAll.html',{'form':form,'iitbhu':'IIT(BHU) Varanasi Registration','formname':user.capitalize()+' Registration form'})

def RAlumni(request):
    return RAll(request,'alumni')
def RStaff(request):
    return RAll(request,'staff')
def RStudent(request):
    return RAll(request,'student')

'''
def RStudent1(request):
    data=[]
    if request.method == 'POST':
        form=['EnrollmentNo','Campus','Department','Program','YearofJoin','Fname','Lname','sex','Marital','DOB','Mobile','Email','Status','Addr1','Addr2','State','Country','PinCode','Password']
        for name in form:
            data.append(request.POST.get(name,''))
        #save data into Student Info Table.........
        SI=StudentInfo(EnrollmentNumber=int(data[0]),Campus=data[1],Department=data[2],Program=data[3],YearofJoin=int(data[4]),Fname=data[5],Lname=data[6],Gender=data[7],MartialStatus=data[8],DOB=data[9],Mob=int(data[10]),EmailId=data[11],CurrentStatus=data[12],Address=data[13]+'\n'+data[14],State=state[int(data[15])-1],Country=data[16],PinCode=int(data[17]))
        try: #Register Use in auth table............
            newuser=auth.models.User.objects.create_user(data[11],data[11],data[18])
            newuser.first_name=data[5]
            newuser.last_name='student'
            newuser.save()
        except:
            return LAuth(request,'red','This Email_Id Alrady Exist','Please Try Register As Another Email_Id')
        SI.save()
        StudentLogin(info=SI,Username1=data[11],Username2=int(data[0])).save()
        return LAuth(request,'green','Welcome '+data[5]+' '+data[6],'You Are Registerd Successfuly')
    return render(request,"RegisterStudent.html",{'iitbhu':'IIT(BHU) Varanasi Registration'})
'''
def Login(request):
    if request.user.id:
        auth.logout(request)
        return HttpResponseRedirect('../Home/')
    data=[]
    if request.method == 'POST':
        usertype={"alumni":AlumniInfo,"staff":StaffInfo,"student":StudentInfo}
        user=request.POST.get('user')
        try:
            user=usertype[request.POST.get('usertype')].objects.filter(Enrollment_Number=int(user))[0].Email_Id
        except Exception as error:
            print(error)
            pass
        usr=auth.authenticate(username=user,password=request.POST.get('pswd'))
        if usr is not None:
            auth.login(request,usr)
        else:
            return HttpResponse('Please Enter Correct Username and Password...')
        try:
            return LAuth(request,'green','Welcome, '+request.user.first_name+' '+User.Last_Name,'Login Successfuly')
        except:
            return LAuth(request,'green','Welcome, '+request.user.first_name,'Login Successfuly')
    return render(request,"Login.html",{})
def RegisteredAlumni(request):
    if request.method=='POST':
        return AlumniProfile(request)
    return render(request,"RegisteredAlumni.html",{})
def Notifications(request):
    if request.user.id:
        return render(request,"News.html",{'creg':'blue','hreg':'','reg':'Welcome, '+request.user.first_name,'log':'LOGOUT','iitbhu':'IIT (BHU) Varanasi News And Notifications'})
    else:
        return LAuth(request,'red','Unauthourised Access','Please Try Login To Access This Page...','IIT (BHU) Varanasi News And Notifications')
def Association(request):
    return HttpResponse("Page Under Construction.......!")
def UpcomingEvents(request):
    return render(request,"header.html",{})
def MakeAGift(request):
    if request.user.id:
        return render(request,"makeagift.html",{'creg':'blue','hreg':'','reg':'Welcome, '+request.user.first_name,'log':'LOGOUT'})
    else:
        return LAuth(request,'red','Unauthourised Access','Please Try Login To Access This Page...')  
def OtherLinks(request):
    return Handle_Pages(request,"OtherLinks.html","IIT BHU(Varanasi) OtherLinks",'')
def chat(request):
    Info={"alumni":AlumniInfo,"staff":StaffInfo,"student":StudentInfo}
    db=sqlite3.connect('chat.db')
    cursor=db.cursor()
    cursor.execute("""select * from chat""")
    chatdata=cursor.fetchall()
    db.close()
    if request.user.id:
        try:
            Info=Info[request.user.last_name].objects.filter(Email_Id=request.user.username)[0]
            return render(request,"chat.html",{'loginuser':Info.First_Name+' '+Info.Last_Name,'chatdata':chatdata})
        except:
            return LAuth(request,'red','Unauthourised Access','Please Try Login To Access This Page...')
    else:
        return LAuth(request,'red','Unauthourised Access','Please Try Login To Access This Page...')
        

