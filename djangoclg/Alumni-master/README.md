# Alumni Website

```
Simple website for IIT BHU Project (Class Project)
```

# Requirements
* Python2.x or Python3.x
* Django == 1.8.9
* Websocket
* Html, Css, JS, Bootstrap

# Run
* python manage.py migrate
* python manage.py createsuperuser
* python manage.py runserver

```
Informative ... (start a website in Django==1.8.9)
```
