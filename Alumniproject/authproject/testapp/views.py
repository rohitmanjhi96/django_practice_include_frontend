from django.shortcuts import render
from django.contrib.auth.decorators import login_required  #logn line to import this
from testapp.forms import signup
from testapp.models import signupmodel
from django.http import HttpResponseRedirect
# Create your views here.
def resultviews(request):
    form = signupmodel.objects.all()
    return render(request,'testapp/result.html',{'form':form})

def homepage_view(request):
    return render(request,'testapp/home.html')

def mainpage_view(request):
    return render(request,'testapp/mainpage.html')

@login_required    #vaid user can open only
def pythonexam_view(request):
    return render(request,'testapp/pythonexams.html')
@login_required
def javaexam_view(request):
    return render(request,'testapp/javaexams.html')

@login_required
def aptitudeexam_view(request):
    return render(request,'testapp/aptitudeexams.html')

def logout_view(request):
    return render(request,'testapp/logout.html')

def signup_view(request):
    form =signup()
    if request.method=='POST':
        form = signup(request.POST)
        #if form.is_valid():
        #    form.save()
        #or
        user = form.save()                #U will not write this code so password will save in encripted format so you will write this code
        user.set_password(user.password)
        user.save()
        return HttpResponseRedirect('/accounts/login')
    return render(request,'testapp/signup.html',{'form':form})
