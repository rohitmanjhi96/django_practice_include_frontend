from django.db import models

# Create your models here.
class signupmodel(models.Model):
    name   = models.CharField(max_length=100)
    contact= models.IntegerField()
    email  = models.EmailField()
    pwd    = models.CharField(max_length=100)
    state  = models.CharField(max_length=100)
    company= models.CharField(max_length=100)
    year   = models.DateField()
    picture= models.ImageField(default = "default.png")

    def __str__(self):
        return self.name
