from django import forms
from django.contrib.auth.models import User
from testapp.models import signupmodel

class signup(forms.ModelForm):
    pwd = forms.CharField(widget=forms.PasswordInput())
    class Meta:
        model=signupmodel
        fields=['name','contact','email','pwd','state','company','year','picture']
