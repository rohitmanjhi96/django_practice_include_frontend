from django.contrib import admin
from testapp.models import signupmodel
# Register your models here.
class signupAdmin(admin.ModelAdmin):
    list_display = ['name','contact','email','pwd','state','company','year','picture']

admin.site.register(signupmodel,signupAdmin)    
